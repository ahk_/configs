set title " change title of terminal to buffer name

set autochdir " automatically change directory to the dir of curent buffer

set hidden " hide buffers instead of closing them

color molokai
" use vim settings rather then vi settings!
set nocompatible

" Some extra stuff to make things easier
set encoding=utf-8
set undofile
set ttyfast
set showmode
set showcmd
set ruler
set ttyfast
set backspace=indent,eol,start
" store loads of commandline history
set history=1000
set undolevels=1000 " huge unde levels



" map ; to : > life saver!!
noremap ; :
noremap , ;
noremap ' "
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required

Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'

" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'

" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}


Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/syntastic'
Plugin 'https://github.com/jlanzarotta/bufexplorer.git'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'tpope/vim-markdown'
Plugin 'https://github.com/vim-scripts/Gundo.git'
Plugin 'https://github.com/sjl/gundo.vim.git'
Plugin 'bling/vim-airline'
Plugin 'Valloric/YouCompleteMe'
Plugin 'majutsushi/tagbar'
Plugin 'flazz/vim-colorschemes'
Plugin 'nvie/flake'
" All of your Plugins must be added before the following line

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

set clipboard=unnamedplus " use system clipboard as default

syntax on " turn on syntax highlighting
syntax enable	" enable syntax processing

set expandtab
set tabstop=4	" number of spaces per tab when display
set softtabstop=4 " number of spaces to tabs when editing
set smarttab

set number " show line number

set showcmd  " show last used command at the bottom

set cursorline " highlight current line

filetype indent on " load filetype-specific indent files

set autoindent

set wildmode=list:longest,full " make cmdline complition similar to bash
set wildmenu  " visual autocomplete for command menu!! AWESOME!!!
set wildignore=*.o,*.obj,*~  " stuff to ignore when tab-completing

" Ignoring case is a fun trick
set ignorecase
set smartcase
set showmatch " hightlight matching [({)}]

set incsearch " searches as the characters are inserted
set hlsearch  " highlight searches

" move vertically by visual line
noremap j gj
noremap k gk

" Use 'sane' regex instead of the vim regex scheme
noremap / /\v
vnoremap / /\v

let mapleader="," " \ is a bit too far away to be leader

" toggle gundo
noremap <F5> :GundoToggle<CR>


" Airline stuff

" to always display the status line
set laststatus=2
" airline stuff
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

let g:airline_theme="powerlineish"

if !exists('g:airline_symbols')
let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
" tell that the terminal has 256 colors
set t_Co=256



"recalculate the trailing whitespace warning when idle, and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

" nerdtree stuff
noremap <C-e> :NERDTreeToggle<cr>

" Tagbar stuff
nore <leader>m :TagbarToggle<CR>

" working with long lines
set nowrap
set textwidth=79
set formatoptions=qrn1
set colorcolumn=79


" save file on loose focus
au FocusLost * :wa

" strip all trailing whitespace
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" fold tag function for HTML files
nnoremap <leader>ft Vatzf

" ctrlp stuff
let g:ctrlp_map='<c-p>'
let g:ctrlp_cmd='CtrlP'

let g:ctrl_p_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }
